# Setting Up a CLASSPATH for Windows Powershell
**Note:** Please use Windows Powershell in COS30018, if you are not confident with using command lines yet. While `cmd` is helpful to know if you need to maintain legacy Windows platforms, it is antiquated. The new Powershell conforms (somewhat) to UNIX/MAC-OS naming conventions, so if you learn Powershell, you'll be primed to understand the UNIX/MAC-OS shells too!

## Setup
If you haven't already done so, create a directory for all your COS30018 projects some where under your home directory on the computer you are using, or on your shared drive. Then download and unzip all the JADE zips. You should end up with *one* "jade/" directory. Note the location of the `jade.jar` file. On my windows system for example it is `C:\Users\spinkus\Documents\cos30018\jade\lib\jade.jar`

Ensure you can launch JADE from the command line:

        java -cp "C:\Users\spinkus\Documents\cos30018\jade\lib\jade.jar" jade.Boot -gui
        
Ensure you can launch JADE with one of the example JADE agents provided in the JADE zips:

        java -cp "C:\Users\spinkus\Documents\cos30018\jade\lib\jade.jar;C:\Users\spinkus\Documents\cos30018\jade\lib\jadeExamples.jar" jade.Boot -gui -agents "mrping:examples.PingAgent.PingAgent"
        
## Setting a CLASSPATH
The `-cp` option to `java` is setting the Java CLASSPATH for the current invocation of Java. Clearly, having to specify that big long class path every time you run JADE is a real PITA. If you don't explicitly specify the `-cp` option Java will look for a class path in the `CLASSPATH` environment variable. If we set that, we can do away with the `-cp` option:

        $env:CLASSPATH="C:\Users\spinkus\Documents\cos30018\jade\lib\jade.jar;C:\Users\spinkus\Documents\cos30018\jade\lib\jadeExamples.jar"
        java jade.Boot -gui -agents "mrping:examples.PingAgent.PingAgent"

To avoid having to set the class path manually every time we start a new Powershell, we need to add the `$env:CLASSPATH=...` line to our Powershell profile file. The profile file is just a script that runs every time you launch a new shell. If we add the `$env:CLASSPATH=...` line to it, every time we start a shell the CLASSPATH is set automatically for the Powershell session. Powershell stores the location of the profile file in the `$profile` variable (it is normally something like `~\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1`) so just type `notepad.exe $profile` from within Powershell to edit this file (choose option to create it if it does not exist). Add the `$env:CLASSPATH=...` line. Close the Powershell, open a new one, and ensure you can run JADE, without the `-cp` option:

        java jade.Boot -gui -agents "mrping:examples.PingAgent.PingAgent"
        
Note, you can see what variables have been set in the environment by typing `dir env:`. See https://technet.microsoft.com/en-us/library/bb613488(v=vs.85).aspx for more on profile files.

## Setting a CLASSPATH to load agents you develop in Eclipse
If you use a run configuration from within Eclipse, Eclipse is smart enough to now where to find the agents you are developing. But if you want to run JADE with some of the agents you are developing from the command line, you need to tell Java where the classes that implement your agents are by adding another path to your class path.

**Scenario:** I set my Eclipse workspace to `C:\Users\spinkus\Documents\cos30018`. I created an Eclipse project called `jade-basics` and added the following simple Agent:

        package cos30018.basics;
        import jade.core.Agent;

        public class MyTestAgent extends Agent {
        	public void setup() {
            System.out.println("TEST AGENT SETTING UP!");
        	}
        }

The full class name of the agent `cos30018.basics.MyTestAgent`. It will be compiled to `C:\Users\spinkus\Documents\cos30018\jade-basics\bin\cos30018\basics\MyTestAgent.class`. To load this agent into JADE from the command line I need to add `C:\Users\spinkus\Documents\cos30018\jade-basics\bin` to the CLASSPATH. To do this we can add "." to your CLASSPATH ("." means the current directory), *and* then change directory to `C:\Users\spinkus\Documents\cos30018\jade-basics\bin`:

        $env:CLASSPATH="C:\Users\spinkus\Documents\cos30018\jade\lib\jade.jar;C:\Users\spinkus\Documents\cos30018\jade\lib\jadeExamples.jar;."
        cd C:\Users\spinkus\Documents\cos30018\jade-basics\bin
        java jade.Boot -gui -agents "mrping:examples.PingAgent.PingAgent;test:cos30018.basics.MyTestAgent"
        
Or we could have done:

        java -cp "$env:CLASSPATH;." jade.Boot -gui -agents "mrping:examples.PingAgent.PingAgent;test:cos30018.basics.MyTestAgent"
        
Or from any directory:

        cd C:\\
        java -cp "$env:CLASSPATH;C:\Users\spinkus\Documents\cos30018\jade-basics\bin\" jade.Boot -gui -agents "mrping:examples.PingAgent.PingAgent;test:cos30018.basics.MyTestAgent"
        
For convenience, edit your Powershell profile as described above to add "." to your CLASSPATH. That way, you just change to the `bin/` directory of your project and it will be on your Java class path.
