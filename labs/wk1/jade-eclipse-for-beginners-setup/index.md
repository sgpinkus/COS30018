<div style="display:none">
<style>
  img { 
    max-width: 75%;
    border: 1px solid #888; 
    margin-left: 60px;
  }
</style>
</div>
# Guide to Installing Jave+Eclipse+JADE

## Step 0 - Find a Computer to Work On! <input type=checkbox />
These instructions are written for Linux and should work with MACs also. In Windows, things work slightly differently. You should still be able to use these instructions as a guide but some of the commands will not execute word-for-word and you'll need to make minor ("common sense") changes:

  - Replace forward slashes ("/") with backslashes ("\") in path names.
  - Use semi-colon (";") to separate path on Java class paths instead of colon (":").
  - In general things are easiest if you use `Powershell`, or the `MINGW` shell, rather than `cmd`.

## Step 1 - Ensure Java Development Kit is Installed <input type=checkbox />
In order to run JADE agents, you need to have a Java Runtime Environment (JRE) installed on your machine. In order to build and compile JADE agents, you need to have the Java Development Kit (JDK) installed as well. A JRE is provided with the Java SDK, so it is sufficient to just have the Java SDK installed.

Ensure you have a recent Java JDK installed by opening a shell and typing the following (">" indicates the command prompt where you type):

        > java -version
        openjdk version "1.8.0_171"
        OpenJDK Runtime Environment (build 1.8.0_171-8u171-b11-1~deb9u1-b11)
        OpenJDK 64-Bit Server VM (build 25.171-b11, mixed mode)
        > javac -version
        javac 1.8.0_171

If you don't see output like above (it is OK if the version is different. >= 1.4 will suffice) then download and install the latest JDK - [http://www.oracle.com/technetwork/java/javase/downloads/index.html](http://www.oracle.com/technetwork/java/javase/downloads/index.html). 

## Step 2 - Ensure Eclipse IDE is Installed <input type=checkbox />
Ensure you have a recent version of Eclipse IDE installed (On Windows press the Windows key then type "eclipse". On Linux/MAC open a shell and type "eclipse").  If you don't then download and install the latest Eclipse IDE - [https://www.eclipse.org/downloads/](http://www.oracle.com/technetwork/java/javase/downloads/index.html).

## Step 3 - Download JADE
First, it's a good idea to create a working directory for the JADE projects you will create this semester (you can name it whatever pleases you):

        > cd ~
        > mkdir COS30018
        > cd COS30018

Now download and unzip JADE into this directory. The current version is `4.5.0` (as at August 2018). JADE comes packaged in four zip files for binaries, source code, documentation, and examples. All of the zip files are provided in a master zip file called JADE-all-4.5.0.zip, so just download this and unzip it. On Linux you can do the following:

**Note:** On MAC you might have to use `tar -xzf <file>` instead of the `unzip` command. On Windows you may have to use a web browser to download the zip files then unzip them via the windows file browser:

        > wget http://jade.tilab.com/dl.php?file=JADE-all-4.5.0.zip -OJADE-all-4.5.0.zip
        > unzip -o JADE-all-4.5.0.zip
        > unzip -o JADE-src-4.5.0.zip
        > unzip -o JADE-examples-4.5.0.zip
        > unzip -o JADE-doc-4.5.0.zip
        > unzip -o JADE-bin-4.5.0.zip
     
You should have one directory called `jade/` that looks like the following:

        > ls jade/
        build.properties  build.xml  classes  demo  doc  lib  License  README  src
        > ls jade/lib/
        commons-codec  jadeExamples.jar  jade.jar

**Note:** If you did not use the command line to unzip the archive, the tool you used may have unpacked the zip files into directories named after the zip file (for example, into a directory  called "JADE-src-4.5.0/jade"). It is fine to leave it like this, but preferable if you rearrange things so everything is under the one `jade/` directory. The following assumes everything is in one `jade/` directory, as above.

**Note:** You can delete or move the zip files somewhere else now if you want.

## Step 4 - Test JADE Agent Framework runs and Setup JAVA CLASSPATH <input type=checkbox />
In order to run JADE from the command line we need to put the two JAR files on our Java CLASSPATH. There are two ways you can do that: with the `-cp` option to `java`, or by setting up your `CLASSPATH` environment variable. Using the the first way, and assuming your current directory contains the `jade/` directory we just unzipped:

### Step 4.1 - It runs
Let's just test we can run JADE first:

        > java -cp "jade/lib/jade.jar" jade.Boot -gui

<img src='img/jade-screen-first.png'></img><br/>

### Step 4.2 - Load some Agents
Now test starting one of the pre-built example Agents in the JADE agent environment. To do this you need to tell JADE where it should look for the agent implementation by putting the JAR file that contains it on the Java class path:

        > java -cp "jade/lib/jade.jar:jade/lib/jadeExamples.jar" jade.Boot -gui -agents "agent1:jade.core.Agent;ping1:examples.PingAgent.PingAgent"
        
<img src='img/jade-screen-w-example-agent.png'></img><br/>

## Step 5 - Working JADE from Eclipse <input type=checkbox />

### 5.1 Create a new Java project

  1. Start up Eclipse.
  2. Create a new project - call it whatever you want, say "jade-getting-started".
  3. Press Finish.
  
<img src='img/create-eclipse-project.png'></img><br/>
  
### 5.2 Include the JADE Libraries
In order to use JADE from an Eclipse Java project you need to include the JADE libraries we downloaded above in our project. We just need to configure where to find the two .jar files called "jade.jar" and "jadeExamples.jar" for now. There are a few ways we can do this in Eclipse. We will create a user library containing these two JAR files, that we can reuse whenever we start a new JADE Java project in Eclipse.

  1. Navigate to *Window >> Preferences >> Java >> Build Path >> User Libraries*
  2. Select *"New Library"* to create our new library, and call it "JADE" (all upper case).
  3. Now select *"Add External JARs ..."* and add the two *"jade.jar"* and *"jadeExamples.jar"* files we downloaded and used above to the library.
  
<img src='img/eclipse-new-user-library.png'></img>

<img src='img/eclipse-new-user-library-2.png'></img><br/>
  
Now we need to add the library we created to our current project:
  
  1. Make sure you only have the one project you created above open, or select the project in the *"Package Explorer"* dock.  
  2. Navigate to *Project >> Properties >> Java Build Path*
  3. Press "Add Library" then select "User Library". Select the library called "JADE" you just created.

### 5.3 Run JADE from a Eclipse "Run Configuration"
Eclipse run configurations are just convenient ways to run command line scripts or a Java programs you are currently developing. Sometimes you want to run a script or the program you are developing with different configurations. Each run time configurations is one such configuration ... 

Create a run-time configuration to do exactly what you did in step 4.1 but from within Eclipse:

  1. Navigate to *Run >> Run Configurations*
  2. Select *"Java Application"* to create a new Java application run configuration.
  3. Give you run configuration a meaningful name like "Launch JADE Environment"
  4. Enter `jade.Boot` as the "Main class".
  5. Enter `-gui -agents "jade.core.Agent"`
  6. Press *Run* to run it and you should see a JADE window appear.

<img src='img/eclipse-run-config-1.png'></img>

<img src='img/eclipse-run-config-2.png'></img>


## Step 6 - Continue Learning About JADE Administration <input type=checkbox />
Please work through the tutorials on [JADE administration and architecture](http://jade.tilab.com/documentation/tutorials-guides/jade-administration-tutorial/#tutorials).
