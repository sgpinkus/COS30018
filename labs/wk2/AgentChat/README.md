# Overview
Very simple chat agent with UI that runs in a JADE agent environment. Agent simply registers to a topic and relays/sends INFORM messages to topic address.


**Run:** Ensure jade.jar, and "." is on your class path. Once compiled, from within the `bin/` directory run:

> java jade.Boot -agents "chatagent:ChatAgent;chattail:ChatTail;chatbot:ChatBot" -services "jade.core.event.NotificationService;jade.core.messaging.TopicManagementService"

**Join chat on another platform:**

> java jade.Boot -container -host <remote-host-address> -agents "chatagent:ChatAgent" -services "jade.core.event.NotificationService;jade.core.messaging.TopicManagementService"

---

![doc/Screenshot_2018-08-31_11-07-38.png](doc/Screenshot_2018-08-31_11-07-38.png)
