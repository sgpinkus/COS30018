import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

import jade.core.AID;
import jade.core.Agent;
import jade.core.ServiceException;
import jade.core.behaviours.TickerBehaviour;
import jade.core.messaging.TopicManagementHelper;
import jade.lang.acl.ACLMessage;
import jade.util.Logger;

public class ChatBot extends Agent {
	private Logger log = Logger.getMyLogger(getClass().getName());
	public final String CHAT_CHANNEL = "CHAT";
	public TopicManagementHelper topicHelper = null;
	public AID topic;

	@Override
	public void setup() {
		log.info("ChatBot::setup()");
		try {
			topicHelper = (TopicManagementHelper) getHelper(TopicManagementHelper.SERVICE_NAME);
			topic = topicHelper.createTopic(CHAT_CHANNEL);
			topicHelper.register(topic); // This idempotent I guess.
		} catch (ServiceException e) {
			log.warning(String.format("Could not create topic [%s]. Agent terminating", e));
			doDelete();
		}
		addBehaviour(new TickerBehaviour(this, 4000) {
			@Override
			protected void onTick() {
				try {
					Process p = Runtime.getRuntime().exec("fortune -s -n80 -o zippy");
					p.waitFor(1, TimeUnit.SECONDS);
					String line = new BufferedReader(new InputStreamReader(p.getInputStream())).readLine();
					log.info(String.format("ChatBot::%s: %s", myAgent.getLocalName(), line));
					send(makeMessage(line));
				} catch (Exception e) {
					log.warning(e.getMessage());
				}
			}

			private ACLMessage makeMessage(String content) {
				ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
				msg.addReceiver(topic);
				msg.setLanguage("English");
				msg.setContent(content);
				return msg;
			}
		});
	}
}
