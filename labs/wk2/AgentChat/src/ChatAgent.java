import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;

import jade.core.AID;
import jade.core.Agent;
import jade.core.ServiceException;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.core.messaging.TopicManagementHelper;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.Logger;

public class ChatAgent extends Agent implements ActionListener {
	private final String CHAT_CHANNEL = "CHAT";
	private Logger log = Logger.getMyLogger(getClass().getName());
	private TopicManagementHelper topicHelper = null;
	private AID topic;
	private ChatWindow window;

	@Override
	public void setup() {
		log.fine("ChatAgent::setup()");
		try {
			topicHelper = (TopicManagementHelper) getHelper(TopicManagementHelper.SERVICE_NAME);
			topic = topicHelper.createTopic(CHAT_CHANNEL);
			topicHelper.register(topic);
		} catch (ServiceException e) {
			log.warning(String.format("Could not create topic [%s]. Agent terminating", e));
			doDelete();
		}
		window = new ChatWindow();
		window.setVisible(true);
		addBehaviour(new ChatChannelListener(window, topic));
		addBehaviour(new UpdateAgentList(this, 1000, window));
		window.textField.addActionListener(this);
	}

	/**
	 * UI event handler to send a message.
	 * @param msg
	 */
	private void sendMessage(String msg) {
		log.fine(String.format("ChatAgent::sendMessage(%s)", msg));
		ACLMessage aclMsg = new ACLMessage(ACLMessage.INFORM);
		aclMsg.addReceiver(topic);
		aclMsg.setLanguage("English");
		aclMsg.setContent(msg);
		send(aclMsg);
	}

	/**
	 * Update UI with any new messages sent to given topic.
	 * @author sam
	 */
	private class ChatChannelListener extends CyclicBehaviour {
		ChatWindow window;
		AID topic;

		ChatChannelListener(ChatWindow window, AID topic) {
			this.window = window;
			this.topic = topic;
		}

		@Override
		public void action() {
			log.fine("ChatChannelListener::action()");
			MessageTemplate tmpl = MessageTemplate
					.and(MessageTemplate.MatchTopic(topic), MessageTemplate.MatchPerformative(ACLMessage.INFORM));
			ACLMessage aclMsg = myAgent.receive(tmpl);
			if (aclMsg != null) {
				String msg = String.format("%s: %s", aclMsg.getSender().getLocalName(), aclMsg.getContent());
				log.fine(msg);
				window.textArea.append(msg + "\n");
			}
			else {
				log.fine("ChatChannelListener::Woken, but no message");
			}
			block();
		}
	}

	/**
	 * Update UI with list of agents currently in platform
	 * @todo Would be nice to just list agents subscribed to topic.
	 * @author sam
	 */
	private class UpdateAgentList extends TickerBehaviour {
		ChatWindow window;

		public UpdateAgentList(Agent a, long period, ChatWindow window) {
			super(a, period);
			this.window = window;
		}

		@Override
		protected void onTick() {
			AMSAgentDescription[] agents = null;
			List<String> excludeFromList = Arrays.asList(new String[] { "df", "ams", "rma" });
			try {
				SearchConstraints c = new SearchConstraints();
				c.setMaxResults(new Long(-1));
				agents = AMSService.search(myAgent, new AMSAgentDescription(), c);
			} catch (Exception e) {
				System.out.println("Problem searching AMS: " + e);
				e.printStackTrace();
			}
			window.textArea_1.setText("");
			for (AMSAgentDescription agent: agents) {
				String name = agent.getName().getLocalName();
				if (excludeFromList.contains(name)) {
					continue;
				}
				window.textArea_1.append(name + "\n");
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String nextMsg = window.textField.getText();
		if (!nextMsg.isEmpty()) {
			sendMessage(window.textField.getText());
			window.textField.setText("");
		}
	}
}
