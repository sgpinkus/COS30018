import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import net.miginfocom.swing.MigLayout;
import javax.swing.JTextField;
import javax.swing.JScrollBar;

public class ChatWindow extends JFrame {
	public JTextArea textArea;
	public JTextArea textArea_1;
	public JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ChatWindow dialog = new ChatWindow();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ChatWindow() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new MigLayout("", "[grow][432px,grow]", "[256px,grow]"));
		JScrollPane scrollPane = new JScrollPane();
		JScrollPane scrollPane_1 = new JScrollPane();
		textArea = new JTextArea();
		textArea_1 = new JTextArea();
		textField = new JTextField();
		scrollPane.setViewportView(textArea);
		scrollPane_1.setViewportView(textArea_1);
		getContentPane().add(scrollPane, "cell 0 0 2 1, growx, growy");
		getContentPane().add(scrollPane_1, "cell 3 0, growx, growy");
		getContentPane().add(textField, "cell 1 1 2 1, shrink, dock south");
		textField.setColumns(100);
		this.setExtendedState(this.getExtendedState() | JFrame.MAXIMIZED_BOTH);
	}
}
