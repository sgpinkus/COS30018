import jade.core.AID;
import jade.core.Agent;
import jade.core.ServiceException;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.messaging.TopicManagementHelper;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.util.Logger;

public class ChatTail extends Agent {
	private final String CHAT_CHANNEL = "CHAT";
	private Logger log = Logger.getMyLogger(getClass().getName());
	private TopicManagementHelper topicHelper = null;
	private AID topic;

	@Override
	public void setup() {
		log.info("ChatAgent::setup()");
		try {
			topicHelper = (TopicManagementHelper) getHelper(TopicManagementHelper.SERVICE_NAME);
			topic = topicHelper.createTopic(CHAT_CHANNEL);
			topicHelper.register(topic);
		} catch (ServiceException e) {
			log.warning(String.format("Could not create topic [%s]. Agent terminating", e));
			doDelete();
		}
		addBehaviour(new CyclicBehaviour() {
			@Override
			public void action() {
				MessageTemplate tmpl = MessageTemplate
						.and(MessageTemplate.MatchTopic(topic), MessageTemplate.MatchPerformative(ACLMessage.INFORM));
				ACLMessage aclMsg = myAgent.receive(tmpl);
				if (aclMsg != null) {
					String msg = String
							.format("ChatTail::%s: %s", aclMsg.getSender().getLocalName(), aclMsg.getContent());
					System.out.println(msg);
				}
				else {
					log.fine("ChatTail::Woken but no message");
				}
				block();
			}
		});
	}
}
