# Overview
This archive contains the following data files:

  - [Climate_HistoricalNormals.tab](Climate_HistoricalNormals.tab)	Climate Historical Normals (1981-2010)
  - [Climate_HourlyWeather.tab](Climate_HourlyWeather.tab)	Climate Hourly Weather Readings
  - [Electricity_P.csv](Electricity_P.csv) Real Power Measurements From All Meters
  - [Electricity_P_DS.csv](Electricity_P_DS.csv) Downsampled version of Electricity_P.csv

The first three data files were copied directly from the [The Almanac of Minutely Power dataset](https://dataverse.harvard.edu/file.xhtml?persistentId=doi:10.7910/DVN/FIE0S4/FS26TX&version=1.2).

`Electricity_P.csv` contains electricity submeter readings form 23 devices, from a single 2 bedroom Candadian house. Readings were taken at interval of 1 minute, over the 2 year of period from April/2012 to March/2014). All electrical meter and sub meter data is in Watts. Please refer to the above link for further details, and citation details if citing this dataset in publications.

The Climate data files are included here for convenience. The content is self descriptive. No other environmental (weather or otherwise) data was recorded in the original experiment.

There are 1,051,200 rows in `Electricity_P.csv` which is ... lots. `Electricity_P_DS.csv` is `Electricity_P.csv` down sampled to 30 minute intervals which makes it a bit more managable. Some date information has also been added, and the first few rows have been removed so that the first row corresponds to 00:00 localtime. The columns of `Electricity_P_DS.csv` are now:

    ABS_INTERVAL_30M,UNIX_TS,WHE,RSE,GRE,MHE,B1E,BME,CWE,DWE,EQE,FRE,HPE,OFE,UTE,WOE,B2E,CDE,DNE,EBE,FGE,HTE,OUE,TVE,UNE,HOUR,MDAY,MIN,MON,WDAY,YDAY,YEAR

See the script `downsample_ampds2.py` for further details.

[1]: https://www.nature.com/articles/sdata201637


Start: 1333263600
End: 1396335540