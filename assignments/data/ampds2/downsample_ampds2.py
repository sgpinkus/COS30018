#!/usr/bin/env python3
'''
This is pre processing script for the "Almanac of Minutely Power dataset (Version 2)" (AMPds2)
"Electricity_P.tab" dataset. It does these things:

  1. Downsamples the data to M minute intervals.
  2. Explodes the time stamp (UNIX_TS column) into a broken down time.

Notes:

  - Source doesn't say what TZ he is in but I think it is Vancouver. Vancouver is UTC-7:00, and
  source says "This integer timestamp is the amount of seconds since 1970-01-01 12:00:00am (UTC)" [1].

[1]: https://www.nature.com/articles/sdata201637
'''
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime, time
import logging
from logging import info
import pytz


logging.getLogger().setLevel(level=logging.INFO)
TZ=-7
M=30
K='ABS_INTERVAL_%dM' % (M,)

def main():
  e = electricity()
  w = weather()
  show(e, e)
  b = e.join(w.set_index(['YEAR', 'MON', 'MDAY', 'HOUR']), how='left', on=['YEAR', 'MON', 'MDAY', 'HOUR'], rsuffix='_W')
  del b['DATETIME_W']
  b.to_csv('Electricity_P_DS.csv', float_format='%.2f', index=False)


def electricity():
  ''' Pre process AMPds2 Electricity_P.csv data set '''
  info('Reading source file')
  df = pd.read_csv('Electricity_P.csv')
  ts0 = df.iloc[0]['UNIX_TS']
  df[K] = df['UNIX_TS'].apply(lambda x: int((x - ts0)/(M*60)))
  df1 = df.loc[:, ['UNIX_TS', K]]
  df2 = df.loc[:, 'WHE':]
  df1 = df1.groupby(K).min().applymap(lambda v: int(v)) # median might be better. OK if M ~ <= 30.
  df2 = df2.groupby(K).mean()
  df = df1.join(df2)
  info('Downsampled to %dm and %d rows', M, len(df))
  info('Exploding time')
  _time = pd.DataFrame([timedict(datetime.fromtimestamp(i, pytz.timezone('Canada/Pacific'))) for i in df['UNIX_TS']]).astype(int)
  _datetime = pd.DataFrame([{ 'datetime': datetime.fromtimestamp(i, pytz.timezone('Canada/Pacific')).strftime('%F %R') } for i in df['UNIX_TS']])
  _time = _datetime.join(_time)
  df = _time.join(df)
  df.columns = map(lambda l: l.upper(), df.columns)
  return df

def weather():
   info('Reading source file')
   w = pd.read_csv('Climate_HourlyWeather.tab', sep='\t')
   w.columns = map(lambda l: l.upper().replace('(', '').replace(')','').replace('/', '').replace(' ', '_').replace('%', 'PCT'), w.columns)
   k = ['DATETIME', 'YEAR', 'MONTH', 'DAY', 'TIME', 'DATA_QUALITY', 'TEMP_C', 'DEW_POINT_TEMP_C', 'REL_HUM_PCT', 'WIND_DIR_10S_DEG', 'WIND_SPD_KMH', 'VISIBILITY_KM', 'STN_PRESS_KPA', 'WEATHER'] # Many cols are all null.
   w = w.filter(k)
   w.rename(columns={'TIME': 'HOUR', 'MONTH': 'MON', 'DAY': 'MDAY'}, inplace=True)
   w['HOUR'] = w['HOUR'].apply(lambda x: time.fromisoformat(x).hour)
   return w

def show(o, s):
  x = 3*24*60
  m = int(x/M)
  plt.plot(o['WHE'][0:x])
  plt.plot(np.arange(0,m)*M, s['WHE'][0:m])
  plt.title('First %d to %d entries before after pro' % (x,m))
  plt.savefig('before-after.png')


def timedict(d):
  ''' del sec and isdst. Sec is always zero, it is never DST '''
  k = ['year', 'mon', 'mday', 'hour', 'min', 'sec', 'wday', 'yday', 'isdst']
  td = dict(zip(k, d.timetuple()))
  del td['sec']
  del td['isdst']
  return td

if __name__ == '__main__':
  main()
